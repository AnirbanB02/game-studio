import React from 'react';
import './styles/app.css';
import Navbar from './components/Navbar';
import EventFlow from './components/EventFlow';
import EventScreen from './components/EventScreen';
import Settings from './components/Settings';

function App() {
  return (
    <div className="App">
      <Navbar />
      <div className="game-studio">
        <EventFlow />
        <EventScreen />
        <Settings />
      </div>
    </div>
  );
}

export default App;
