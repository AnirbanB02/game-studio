import React from 'react';
import '../styles/navbar.css';

const Navbar = () => {
  const d = new Date();
  let hours = d.getHours();
  let am_pm = 'am';
  if (hours > 12) {
    am_pm = 'pm';
    hours -= 12;
  }
  return (
    <div className='Navbar'>
      <div>
        <button className='button-trans'> &lt; </button>
        <button className='button-trans'>Participants</button>
      </div>
      <p className='nav-title'>STUDIO - Pictionary for Swiggy - Group 3</p>
      <div className='current-time'>
        {d.getDate()}-{d.getMonth() + 1}-{d.getFullYear()} <br /> {hours}:
        {d.getMinutes()} {am_pm} IST
      </div>
    </div>
  );
};

export default Navbar;
