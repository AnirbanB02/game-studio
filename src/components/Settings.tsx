import React from 'react';
import '../styles/settings.css';
import MuteOptions from './settingsComponents/MuteOptions';
import SettingsButtons from './settingsComponents/SettingsButtons';
import SettingsConfig from './settingsComponents/SettingsConfig';

const Settings = () => {
  return (
    <div className='Settings'>
      <div className="subheader"> ANSWER SETTINGS</div>
      <div className="settings-content">
        <SettingsButtons />
        <SettingsConfig />
        <MuteOptions />
      </div>
    </div>
  )
}

export default Settings
