import React from 'react';
import { Button } from '@mui/material';
// import {makeStyles} from '@mui/material/styles';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { makeStyles } from '@mui/styles';

const useStyles: any = makeStyles((theme: any) => ({
  icon: {
    color: '#B2B2B2',
  },
  settingButton: {
    backgroundColor: '#5B8DB2',
    '&:hover':{
      backgroundColor: 'magenta',
    }
  },
  settingButtonActive: {
    backgroundColor: 'magenta',
    '&:hover':{
      backgroundColor: 'magenta'
    }
  }
}));

const SettingsButtons = () => {
  const classes = useStyles();

  return (
    <div className='settings-buttons'>
      <Button
        className={classes.settingButtonActive}
        variant='contained'
        startIcon={<></>}
        endIcon={<ArrowRightIcon className={classes.icon} />}
      >
        Player
      </Button>
      <Button
        className={classes.settingButton}
        variant='contained'
        startIcon={<></>}
        endIcon={<ArrowRightIcon className={classes.icon} />}
      >
        Playing Team
      </Button>
      <Button
        className={classes.settingButton}
        variant='contained'
        startIcon={<></>}
        endIcon={<ArrowRightIcon className={classes.icon} />}
      >
        Other Team
      </Button>
      <Button
        className={classes.settingButton}
        variant='contained'
        startIcon={<></>}
        endIcon={<ArrowRightIcon className={classes.icon} />}
      >
        Global
      </Button>
      
    </div>
  );
};

export default SettingsButtons;
