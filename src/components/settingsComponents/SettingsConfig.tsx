import React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { makeStyles } from '@mui/styles';
import { Typography } from '@mui/material';
import MicIcon from '@mui/icons-material/Mic';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';
import ForumOutlinedIcon from '@mui/icons-material/ForumOutlined';

const useStyles = makeStyles((theme) => ({
  demo: {
    color: '#E5E5E5',
  },
  dropdown: {
    backgroundColor: '#5B8DB2',
    color: '#FFFFFF',
    width: '200px',
    fontSize:'1.3rem',
    borderRadius: '20px'
  },
}));

const SettingsConfig = () => {
  const [micState, setMicState] = React.useState('');

  const handleChangeMic = (event: SelectChangeEvent) => {
    setMicState(event.target.value);
  };
  const [spotlight, setSpotlight] = React.useState('');

  const handleChangeSpotlight = (event: SelectChangeEvent) => {
    setSpotlight(event.target.value);
  };
  const [whiteboard, setWhiteboard] = React.useState('');

  const handleChangeWhiteboard = (event: SelectChangeEvent) => {
    setWhiteboard(event.target.value);
  };
  const [chat, setChat] = React.useState('');

  const handleChangeChat = (event: SelectChangeEvent) => {
    setChat(event.target.value);
  };
  const classes = useStyles();
  return (
    <div className='settings-config'>
      <div className={classes.demo}>
        <Typography variant='h6'>
          <MicIcon /> Microphone
        </Typography>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <Select
            value={micState}
            onChange={handleChangeMic}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
            className={classes.dropdown}
          >
            <MenuItem value={undefined}>
              <em>Select</em>
            </MenuItem>
            <MenuItem value={0}>Mute</MenuItem>
            <MenuItem value={1}>Enable</MenuItem>
            <MenuItem value={2}>Disabled</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className={classes.demo}>
        <Typography variant='h6'>
          <AccountCircleOutlinedIcon /> Spotlight
        </Typography>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <Select
            value={spotlight}
            onChange={handleChangeSpotlight}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
            className={classes.dropdown}
          >
            <MenuItem value={undefined}>
              <em>Select</em>
            </MenuItem>
            <MenuItem value={0}>Primary</MenuItem>
            <MenuItem value={1}>Secondary</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className={classes.demo}>
        <Typography variant='h6'>
          <ConnectedTvIcon /> Whiteboard
        </Typography>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <Select
            value={whiteboard}
            onChange={handleChangeWhiteboard}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
            className={classes.dropdown}
          >
            <MenuItem value={undefined}>
              <em>Select</em>
            </MenuItem>
            <MenuItem value={0}>Primary</MenuItem>
            <MenuItem value={1}>Secondary</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className={classes.demo}>
        <Typography variant='h6'>
          <ForumOutlinedIcon /> Chat
        </Typography>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <Select
            value={chat}
            onChange={handleChangeChat}
            displayEmpty
            inputProps={{ 'aria-label': 'Without label' }}
            className={classes.dropdown}
          >
            <MenuItem value={undefined}>
              <em>Select</em>
            </MenuItem>
            <MenuItem value={0}>Primary</MenuItem>
            <MenuItem value={1}>Secondary</MenuItem>
          </Select>
        </FormControl>
      </div>
    </div>
  );
};

export default SettingsConfig;
