import { Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import React from 'react'
const useStyles = makeStyles({
  muteHead:{
    color: '#E5E5E5',
    textDecoration:'underline',
    textAlign: 'left'
  },
  muteContent:{
    color:'#E5E5E5',
    textAlign: 'left',
    margin:'10px 5px'
  }
});

const MuteOptions = () => {
  const classes = useStyles();
  return (
    <div className='mute-options'>
      <Typography variant='h4' className={classes.muteHead}> Mute Options</Typography>
      <Typography variant='h5' className={classes.muteContent}> Mute: Player is muted and is not allowed to unmute themselves</Typography>
      <Typography variant='h5' className={classes.muteContent}> Enabled: Player is allowed to mute and unmute themselves</Typography>
      <Typography variant='h5' className={classes.muteContent}> Disabled: Player is unable to mute themselves and will remain umnuted</Typography>
    </div>
  )
}

export default MuteOptions
