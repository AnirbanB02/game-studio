import React from 'react';
import '../styles/eventscreen.css';
import laptop from '../assets/images/laptop-demo.png';

const EventScreen = () => {
  return (
    <div className='EventScreen'>
      <div className="subheader">EVENT SCREEEN</div>
      <div id='laptop'>
        <img src={laptop} alt='laptop-demo-screen' />
        <div id="green-screen"></div>
      </div>
    </div>
  );
};

export default EventScreen;
