import * as React from 'react';
import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import DraftsIcon from '@mui/icons-material/Drafts';
import SendIcon from '@mui/icons-material/Send';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import StarBorder from '@mui/icons-material/StarBorder';
import '../styles/eventflow.css';
import Avatar from '@mui/material/Avatar';
import WelcomeVideoIcon from '../assets/images/welcomeVideoIcon.png';
import InstructionVideoIcon from '../assets/images/InstructionVideoIcon.png';
import TeamSelectionIcon from '../assets/images/TeamSelectionIcon.png';
import Round1Icon from '../assets/images/Round1Icon.png';
import Turns from '../assets/images/Turns.png';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  eventFlowIcon: {
    color: 'rgba(255,255,255,0.7)',
  },
});

const EventFlow = () => {
  const [open1, setOpen1] = React.useState(true);

  const handleClick1 = () => {
    setOpen1(!open1);
  };
  const [open2, setOpen2] = React.useState(true);

  const handleClick2 = () => {
    setOpen2(!open2);
  };
  const classes = useStyles();
  return (
    <div className='EventFlow'>
      <div className='subheader'>EVENT FLOW</div>
      <List
        sx={{ width: '100%', maxWidth: 720 }}
        component='nav'
        className='list-mui'
      >
        <ListItemButton>
          <ListItemIcon>
            <img alt='' src={WelcomeVideoIcon} />
          </ListItemIcon>
          <ListItemText primary='Welcome Video' />
        </ListItemButton>
        <ListItemButton>
          <ListItemIcon>
            <img alt='' src={InstructionVideoIcon} />
          </ListItemIcon>
          <ListItemText primary='Instruction Video' />
        </ListItemButton>
        <ListItemButton>
          <ListItemIcon>
            <img alt='' src={TeamSelectionIcon} />
          </ListItemIcon>
          <ListItemText primary='Team Selection' />
        </ListItemButton>

        <ListItemButton onClick={handleClick1}>
          <ListItemIcon>
            <img alt='' src={Round1Icon} />
          </ListItemIcon>
          <ListItemText primary='Round 1' />
          {open1 ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={open1} timeout='auto' unmountOnExit>
          <List component='div' disablePadding>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon>
                <FormatListBulletedIcon className={classes.eventFlowIcon} />
              </ListItemIcon>
              <ListItemText primary='Instructions' />
            </ListItemButton>
            <ListItemButton onClick={handleClick2}>
              <ListItemIcon>
                <img alt='' src={Turns} />
              </ListItemIcon>
              <ListItemText primary='Turns' />
              {open1 ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={open2} timeout='auto' unmountOnExit>
              <List component='div' disablePadding>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemIcon>
                    <FormatListBulletedIcon className={classes.eventFlowIcon} />
                  </ListItemIcon>
                  <ListItemText primary='Player Select' />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemIcon>
                    <FormatListBulletedIcon className={classes.eventFlowIcon} />
                  </ListItemIcon>
                  <ListItemText primary='Player Select' />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemIcon>
                    <FormatListBulletedIcon className={classes.eventFlowIcon} />
                  </ListItemIcon>
                  <ListItemText primary='Player Select' />
                </ListItemButton>
                <ListItemButton sx={{ pl: 4 }}>
                  <ListItemIcon>
                    <FormatListBulletedIcon className={classes.eventFlowIcon} />
                  </ListItemIcon>
                  <ListItemText primary='Player Select' />
                </ListItemButton>
              </List>
            </Collapse>
          </List>
        </Collapse>
      </List>
    </div>
  );
};

export default EventFlow;
